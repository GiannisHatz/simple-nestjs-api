import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  KeycloakConnectOptions,
  KeycloakConnectOptionsFactory,
  PolicyEnforcementMode,
  TokenValidation,
} from 'nest-keycloak-connect';

@Injectable()
export class KeycloakConfigService implements KeycloakConnectOptionsFactory {
  constructor(private readonly _configService: ConfigService) {}

  createKeycloakConnectOptions(): KeycloakConnectOptions {
    return {
      authServerUrl: this._configService.get('KEYCLOAK_AUTH_URL'),
      realm: this._configService.get('KEYCLOAK_REALM'),
      clientId: this._configService.get('KEYCLOAK_CLIENT_ID'),
      secret: this._configService.get('KEYCLOAK_CLIENT_SECRET'),
      cookieKey:
        this._configService.get('KEYCLOAK_COOKIE_KEY') || 'KEYCLOAK_JWT',
      policyEnforcement: PolicyEnforcementMode.PERMISSIVE,
      tokenValidation: TokenValidation.ONLINE,
    };
  }
}
